const API_HOST = 'http://35.246.9.208';
const PORT = '3000';

export const environment = {
  production: false,
  API_HOST,
  PORT,
  API_URL: `${API_HOST}:${PORT}/`,
  TOKEN_LOCAL_STORAGE_KEY: '__token__',
};
