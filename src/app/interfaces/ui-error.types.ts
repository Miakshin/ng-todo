export interface IUiError {
  title?: string;
  text: string;
}
