import { PipeTransform, Pipe } from '@angular/core';
import { isUndefined } from 'util';

@Pipe({ name: 'filterBySubject' })
export class FilterBySubjectPipe implements PipeTransform {
  transform(dataList: any[], subject: string, value?: any) {
    if (isUndefined(value)) {
      return dataList;
    }

    return dataList.filter(dataItem => dataItem[subject] === value);
  }
}
