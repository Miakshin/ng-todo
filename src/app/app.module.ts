import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpClientModule,
} from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { UserModule } from './modules/user/user.module';
import { MatCardModule } from '@angular/material/card';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AuthService } from './modules/auth/auth.service';
import { AuthStore } from './store/auth.store';
import { RegistrationSuccessPageComponent } from './modules/auth/components/registration-success-page/registration-success-page.component';
import { ConfirmEmailPageComponent } from './modules/auth/components/confirm-email-page/confirm-email-page.component';
import { MatTabsModule } from '@angular/material';
import { SharedUiComponentsModule } from './modules/shared-ui-components/shared-ui-components.module';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationSuccessPageComponent,
    ConfirmEmailPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    UserModule,
    MatCardModule,
    HttpClientModule,
    FontAwesomeModule,
    MatTabsModule,
    SharedUiComponentsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    HttpClient,
    AuthService,
    AuthStore,
  ],
  bootstrap: [AppComponent],
  exports: [FontAwesomeModule],
})
export class AppModule {}
