import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { AuthStore } from '../store/auth.store';
import 'rxjs/add/operator/do';
import { AuthService } from '../modules/auth/auth.service';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private readonly authStore: AuthStore,
    private readonly authService: AuthService,
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const authToken = this.authStore.getToken();
    if (authToken) {
      req = req.clone({ setHeaders: { Authorization: authToken } });
    }

    req = req.clone({
      url: environment.API_URL + req.url,
    });

    return next.handle(req).do(
      (event: HttpEvent<any>) => {},
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.authService.logout();
          }
        }
      },
    );
  }
}
