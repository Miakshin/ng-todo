import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthStore {
  token: BehaviorSubject<string>;

  constructor() {
    const storedToken =
      localStorage.getItem(environment.TOKEN_LOCAL_STORAGE_KEY) || '';
    this.token = new BehaviorSubject(storedToken);
  }

  public setToken(token: string) {
    localStorage.setItem(environment.TOKEN_LOCAL_STORAGE_KEY, token);
    this.token.next(token);
  }

  public getToken(): string {
    return this.token.value;
  }
}
