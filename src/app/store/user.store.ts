import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IUser } from '../modules/user/user.type';

@Injectable()
export class UserStore {
  user: BehaviorSubject<IUser | undefined>;

  constructor() {
    this.user = new BehaviorSubject<IUser | undefined>(undefined);
  }

  public setUser(userData: IUser) {
    this.user.next(userData);
  }

  public getUser(): IUser | undefined {
    return this.user.value;
  }
}
