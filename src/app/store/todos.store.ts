import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ITodo } from '../modules/todos/todo.type';

@Injectable()
export class TodosStore {
  todos: BehaviorSubject<ITodo[]>;

  constructor() {
    this.todos = new BehaviorSubject<ITodo[]>([]);
  }

  public setTodos(todos: ITodo[]) {
    this.todos.next(todos);
  }

  public getTodos(): ITodo[] {
    return this.todos.value;
  }
}
