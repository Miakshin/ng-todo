import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-loading-layer',
  templateUrl: './loading-layer.component.html',
  styleUrls: ['./loading-layer.component.css'],
})
export class LoadingLayerComponent {
  @Input()
  diameter = 48;
}
