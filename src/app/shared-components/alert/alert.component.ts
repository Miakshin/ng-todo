import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  faTimes,
  faExclamationCircle,
  faCheckCircle,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css'],
})
export class AlertComponent {
  @Input()
  type: 'success' | 'error';
  @Input()
  title?: string;
  @Input()
  text: string;
  @Output()
  closeAlert: EventEmitter<void> = new EventEmitter();

  faTimes = faTimes;
  alertTypeIcon = this.type === 'success' ? faCheckCircle : faExclamationCircle;

  onAlertClose = () => this.closeAlert.emit();
}
