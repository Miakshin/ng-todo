export interface ISignInRO {
  email: string;
  password: string;
}

export interface ISignUpRO {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}

export interface ISignInResponse {
  token: string;
  ttl: number;
}
