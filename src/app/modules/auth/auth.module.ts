import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SignInComponent } from './components/auth-page/sign-in/sign-in.component';
import { SignUpComponent } from './components/auth-page/sign-up/sign-up.component';
import {
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatTabsModule,
  MatProgressSpinnerModule,
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AuthRoutingModule } from './auth-routes.module';
import { SharedUiComponentsModule } from '../shared-ui-components/shared-ui-components.module';
import { AuthPageComponent } from './components/auth-page/auth-page.component';

@NgModule({
  declarations: [AuthPageComponent, SignInComponent, SignUpComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    FontAwesomeModule,
    AuthRoutingModule,
    SharedUiComponentsModule,
  ],
  providers: [],
})
export class AuthModule {}
