import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISignInRO, ISignInResponse, ISignUpRO } from './auth.types';
import {
  AUTH_SIGN_IN_URL,
  AUTH_REFRESH_URL,
  AUTH_SIGN_UP_URL,
  REGISTRATION_SUCCESS,
  AUTH_CONFIRM_REGISTRATION,
} from './auth.constants';
import { AuthStore } from '../../store/auth.store';
import { Router } from '@angular/router';
import { TODOS_ROUTE_PREFIX } from '../../app.constants';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  secondsBeforeTokenExpired: number;
  refreshTimeoutIds: number[];

  constructor(
    private readonly http: HttpClient,
    private readonly authStore: AuthStore,
    private readonly router: Router,
  ) {
    this.secondsBeforeTokenExpired = 120;
    this.refreshTimeoutIds = [];
  }

  signIn(credentials: ISignInRO): Promise<ISignInResponse> {
    return new Promise((resolve, reject) => {
      this.http
        .post(AUTH_SIGN_IN_URL, credentials)
        .subscribe((tokenData: ISignInResponse) => {
          this.updateToken(tokenData);
          this.router.navigate([TODOS_ROUTE_PREFIX]);
          resolve(tokenData);
        }, reject);
    });
  }

  signUp(signUpData: ISignUpRO): Promise<null> {
    return new Promise((resolve, reject) => {
      this.http.post(AUTH_SIGN_UP_URL, signUpData).subscribe((data: null) => {
        this.router.navigate([REGISTRATION_SUCCESS]);
        resolve(data);
      }, reject);
    });
  }

  refreshToken() {
    const token = this.authStore.getToken();
    if (token) {
      this.http.get(AUTH_REFRESH_URL).subscribe(this.updateToken.bind(this));
    }
  }

  checkEmailConfirmationToken = (token: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.http
        .post(AUTH_CONFIRM_REGISTRATION, { token })
        .subscribe(({ email }: { email: string }) => resolve(email), reject);
    });
  };

  logout() {
    this.authStore.setToken('');
    this.clearTokenRefreshTimeout();
    this.router.navigate([AUTH_SIGN_IN_URL]);
  }

  private updateToken({ token, ttl }: ISignInResponse) {
    this.authStore.setToken(token);
    const refreshTime = (ttl - this.secondsBeforeTokenExpired) * 1000;
    const refreshTimeoutId = setTimeout(
      this.refreshToken.bind(this),
      refreshTime,
    );
    this.refreshTimeoutIds = [...this.refreshTimeoutIds, refreshTimeoutId];
  }

  private clearTokenRefreshTimeout() {
    for (const timeoutId of this.refreshTimeoutIds) {
      clearTimeout(timeoutId);
    }
  }
}
