import { Component, OnInit } from '@angular/core';
import { AUTH_SIGN_IN_URL } from '../../auth.constants';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-registration-success-page',
  templateUrl: './registration-success-page.component.html',
  styleUrls: ['./registration-success-page.component.css'],
})
export class RegistrationSuccessPageComponent implements OnInit {
  faPaperPlane = faPaperPlane;
  loginLink = AUTH_SIGN_IN_URL;
  constructor() {}

  ngOnInit() {}
}
