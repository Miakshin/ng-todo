import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth.service';
import { AUTH_SIGN_IN_URL } from '../../auth.constants';
import { faThumbsUp, faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-confirm-email-page',
  templateUrl: './confirm-email-page.component.html',
  styleUrls: [
    './confirm-email-page.component.css',
    '../../../../styles/icon.styles.css',
  ],
})
export class ConfirmEmailPageComponent implements OnInit {
  loginLink = AUTH_SIGN_IN_URL;
  faThumbUp = faThumbsUp;
  faTimes = faTimes;
  loading = false;
  error: string;
  successText: string;
  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly authService: AuthService,
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParamMap.subscribe(params => {
      const token = params.get('token');
      if (token) {
        this.checkToken(token);
      }
    });
  }

  private setSuccessText = (email: string) => {
    console.log(email);
    this.successText = `${email} address have confirmed!`;
  };

  private setError = () => {
    this.error = 'Invalid link. This email possible has been activated.';
  };

  checkToken = (token: string) => {
    this.loading = true;
    this.authService
      .checkEmailConfirmationToken(token)
      .then(this.setSuccessText)
      .catch(this.setError)
      .finally(() => (this.loading = false));
  };
}
