import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InputErrorMatcher } from 'src/app/directives/input-error-matcher.directive';
import { AuthService } from '../../../auth.service';
import { IUiError } from '../../../../../interfaces/ui-error.types';
import { HttpErrorResponse } from '@angular/common/http';
import { authErrors } from '../../../auth.constants';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: [
    './sign-in.component.css',
    '../../../../../styles/input.styles.css',
  ],
})
export class SignInComponent implements OnInit {
  loading: boolean;
  signInForm: FormGroup;
  error: IUiError | null = null;

  constructor(private readonly authService: AuthService) {
    this.loading = false;
  }

  inputErrorMatcher = new InputErrorMatcher();

  ngOnInit() {
    this.signInForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
      ]),
    });
  }

  login() {
    this.loading = true;
    this.authService
      .signIn(this.signInForm.value)
      .catch(this.handleError)
      .finally(() => (this.loading = false));
  }

  cleanError = () => (this.error = null);

  handleError = (error: HttpErrorResponse) => {
    const code = error.status;
    switch (code) {
      case 403:
        this.error = authErrors[403];
        break;
      case 400:
        this.error = authErrors[400];
        break;
      default:
        this.error = authErrors[500];
    }
  };
}
