import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InputErrorMatcher } from '../../../../../directives/input-error-matcher.directive';
import { AuthService } from '../../../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: [
    './sign-up.component.css',
    '../../../../../styles/input.styles.css',
  ],
})
export class SignUpComponent implements OnInit {
  loading: boolean;
  signUpForm: FormGroup;
  constructor(private readonly authService: AuthService) {}

  inputErrorMatcher = new InputErrorMatcher();

  ngOnInit() {
    this.signUpForm = new FormGroup({
      firstName: new FormControl('', [
        Validators.minLength(4),
        Validators.required,
      ]),
      lastName: new FormControl('', [
        Validators.minLength(4),
        Validators.required,
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
      ]),
    });
  }

  onFormSubmit() {
    this.loading = true;
    this.authService.signUp(this.signUpForm.value).finally(() => {
      this.loading = false;
    });
  }
}
