import { Component } from '@angular/core';
import { SIGN_IN, SIGN_UP } from '../../auth.constants';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.css'],
})
export class AuthPageComponent {
  signInLink = SIGN_IN;
  signUpLink = SIGN_UP;
}
