import { RouterModule, Routes } from '@angular/router';
import { SIGN_IN, SIGN_UP } from './auth.constants';
import { SignInComponent } from './components/auth-page/sign-in/sign-in.component';
import { SignUpComponent } from './components/auth-page/sign-up/sign-up.component';
import { NgModule } from '@angular/core';
import { AuthPageComponent } from './components/auth-page/auth-page.component';

const routes: Routes = [
  {
    path: '',
    component: AuthPageComponent,
    children: [
      { path: SIGN_IN, component: SignInComponent },
      { path: SIGN_UP, component: SignUpComponent },
      { path: '**', redirectTo: SIGN_IN },
    ],
  },
  { path: '**', redirectTo: SIGN_IN },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
