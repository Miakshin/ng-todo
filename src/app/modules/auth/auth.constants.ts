import { AUTH_ROUTE_PREFIX } from 'src/app/app.constants';
import { IUiError } from '../../interfaces/ui-error.types';

export const SIGN_IN = 'sign-in';
export const SIGN_UP = 'sign-up';
export const REGISTRATION_SUCCESS = 'registration-success';
export const CONFIRM_REGISTRATION = 'confirm-registration';

export const AUTH_ENDPOINT = `${AUTH_ROUTE_PREFIX}/`;
export const AUTH_SIGN_IN_URL = `${AUTH_ENDPOINT}${SIGN_IN}`;
export const AUTH_SIGN_UP_URL = `${AUTH_ENDPOINT}${SIGN_UP}`;
export const AUTH_REFRESH_URL = `${AUTH_ENDPOINT}refresh`;
export const AUTH_CONFIRM_REGISTRATION = `${AUTH_ENDPOINT}${CONFIRM_REGISTRATION}`;

export const SIGN_IN_ROUTE = `${AUTH_ROUTE_PREFIX}/${SIGN_IN}`;
export const SIGN_UP_ROUTE = `${AUTH_ROUTE_PREFIX}/${SIGN_UP}`;

export const authErrors: { [key: string]: IUiError } = {
  403: {
    text: 'This account in not activated',
  },
  400: {
    text: 'Wrong email or password',
  },
  500: {
    text: 'Unexpected error',
  }
};
