import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from '../../shared-components/alert/alert.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LoadingLayerComponent } from '../../shared-components/loading-layer/loading-layer.component';
import { MatProgressSpinnerModule } from '@angular/material';

@NgModule({
  declarations: [AlertComponent, LoadingLayerComponent],
  imports: [CommonModule, FontAwesomeModule, MatProgressSpinnerModule],
  exports: [AlertComponent, LoadingLayerComponent, MatProgressSpinnerModule],
})
export class SharedUiComponentsModule {}
