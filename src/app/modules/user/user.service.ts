import { Injectable } from '@angular/core';
import { UserStore } from '../../store/user.store';
import { HttpClient } from '@angular/common/http';
import { USER_CURRENT_URL } from './user.constants';
import { IUser } from './user.type';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private userStore: UserStore, private http: HttpClient) {}

  public fetchUserData(): Subscription {
    return this.http.get(USER_CURRENT_URL).subscribe(this.updateUserData);
  }

  private updateUserData(user: IUser) {
    return this.userStore.setUser(user);
  }
}
