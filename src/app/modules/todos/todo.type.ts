export interface ITodo {
  id: number;
  created: string;
  updated: string;
  title?: string;
  text: string;
  done: boolean;
}
