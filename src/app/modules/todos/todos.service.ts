import { Injectable } from '@angular/core';
import { TodosStore } from 'src/app/store/todos.store';
import { HttpClient } from '@angular/common/http';
import {
  TODO_CREATE_URL,
  TODO_ENDPOINT,
  TODO_LIST_URL,
} from './todos.constants';
import { ITodo } from './todo.type';
import { ITodoCreateRO, ITodoUpdateRO } from './todo-ro.types';

@Injectable({
  providedIn: 'root',
})
export class TodosService {
  constructor(
    private todosStore: TodosStore,
    private readonly http: HttpClient,
  ) {}

  public fetchTodoList(): Promise<ITodo[]> {
    return new Promise((resolve, reject) => {
      this.http.get(TODO_LIST_URL).subscribe((todos: ITodo[]) => {
        this.updateTodos(todos);
        resolve(todos);
      }, reject);
    });
  }

  public updateTodo(id: number, data: ITodoUpdateRO): Promise<ITodo> {
    return new Promise((resolve, reject) => {
      this.http.put(`${TODO_ENDPOINT}${id}`, data).subscribe((todo: ITodo) => {
        const todos = this.todosStore.getTodos();
        const prevTodoIndex = todos.findIndex(t => t.id === todo.id);
        prevTodoIndex >= 0 ? (todos[prevTodoIndex] = todo) : todos.push(todo);
        this.updateTodos(todos);
        resolve(todo);
      }, reject);
    });
  }

  public createTodo(data: ITodoCreateRO): Promise<ITodo> {
    return new Promise((resolve, reject) => {
      this.http.post(TODO_CREATE_URL, data).subscribe((todo: ITodo) => {
        const todos = this.todosStore.getTodos();
        this.updateTodos([...todos, todo]);
        resolve(todo);
      }, reject);
    });
  }

  public deleteTodo(id: number): Promise<undefined> {
    return new Promise((resolve, reject) => {
      this.http.delete(`${TODO_ENDPOINT}${id}`).subscribe(() => {
        const todos = this.todosStore.getTodos();
        const updatedTodos = todos.filter(todo => todo.id !== id);
        this.todosStore.setTodos(updatedTodos);
        resolve(undefined);
      }, reject);
    });
  }

  private updateTodos(todos: ITodo[]) {
    this.todosStore.setTodos(todos);
  }
}
