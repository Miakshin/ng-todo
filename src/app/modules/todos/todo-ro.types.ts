export interface ITodoCreateRO {
  title?: string;
  text: string;
}

export interface ITodoUpdateRO extends ITodoCreateRO {
  done: boolean;
}
