import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoPageComponent } from './components/todo-page/todo-page.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatTabsModule,
  MatDialogModule,
} from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';
import { CreateTodoComponent } from './components/todo-page/create-todo/create-todo.component';
import { TodoListComponent } from './components/todo-page/todo-list/todo-list.component';
import { TodoComponent } from './components/todo-page/todo-list/todo/todo.component';
import { TodosService } from './todos.service';
import { TodosStore } from '../../store/todos.store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterBySubjectPipe } from 'src/app/pipes/filter-by-subject.pipe';
import { TodoUpdateDialogComponent } from './components/todo-page/todo-list/todo-update-dialog/todo-update-dialog.component';
import { TodoFormFieldsComponent } from './components/todo-page/todo-form-fields/todo-form-fields.component';
import { SharedUiComponentsModule } from '../shared-ui-components/shared-ui-components.module';
import { TodosRoutingModule } from './todos-routing.module';

@NgModule({
  declarations: [
    TodoPageComponent,
    CreateTodoComponent,
    TodoListComponent,
    TodoComponent,
    FilterBySubjectPipe,
    TodoUpdateDialogComponent,
    TodoFormFieldsComponent,
  ],
  imports: [
    CommonModule,
    TodosRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    FontAwesomeModule,
    RouterModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatTabsModule,
    MatDialogModule,
    SharedUiComponentsModule,
  ],
  entryComponents: [TodoUpdateDialogComponent],
  providers: [TodosService, TodosStore],
})
export class TodosModule {}
