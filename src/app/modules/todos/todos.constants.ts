export const TODO_ENDPOINT = 'todo/';
export const TODO_LIST_URL = `${TODO_ENDPOINT}list`;
export const TODO_CREATE_URL = `${TODO_ENDPOINT}create`;
