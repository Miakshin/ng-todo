import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InputErrorMatcher } from '../../../../../directives/input-error-matcher.directive';
import { TodosService } from '../../../todos.service';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: [
    './create-todo.component.css',
    '../../../../../styles/input.styles.css',
    '../../../../../styles/card.styles.css',
  ],
})
export class CreateTodoComponent implements OnInit {
  createTodoForm: FormGroup;
  inputErrorMatcher = new InputErrorMatcher();
  loading = false;

  constructor(private readonly todosService: TodosService) {}

  ngOnInit() {
    this.createTodoForm = new FormGroup({
      title: new FormControl(''),
      text: new FormControl('', [
        Validators.required,
        Validators.maxLength(260),
      ]),
    });
  }

  createTodo() {
    this.loading = true;
    this.todosService
      .createTodo(this.createTodoForm.value)
      .then(() => this.createTodoForm.reset())
      .finally(() => (this.loading = false));
  }
}
