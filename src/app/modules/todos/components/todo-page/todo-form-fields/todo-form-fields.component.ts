import { Component, Input } from '@angular/core';
import { InputErrorMatcher } from '../../../../../directives/input-error-matcher.directive';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-todo-form-fields',
  templateUrl: './todo-form-fields.component.html',
  styleUrls: [
    './todo-form-fields.component.css',
    '../../../../../styles/input.styles.css',
  ],
})
export class TodoFormFieldsComponent {
  @Input()
  inputErrorMatcher: InputErrorMatcher;

  @Input()
  form: FormGroup;
}
