import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoFormFieldsComponent } from './todo-form-fields.component';

describe('TodoFormFieldsComponent', () => {
  let component: TodoFormFieldsComponent;
  let fixture: ComponentFixture<TodoFormFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TodoFormFieldsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoFormFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
