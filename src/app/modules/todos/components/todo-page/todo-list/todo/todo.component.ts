import { Component, Input } from '@angular/core';
import { ITodo } from 'src/app/modules/todos/todo.type';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons/faTrashAlt';
import { faEdit } from '@fortawesome/free-solid-svg-icons/faEdit';
import { TodosService } from '../../../../todos.service';
import { MatDialog } from '@angular/material/dialog';
import { TodoUpdateDialogComponent } from '../todo-update-dialog/todo-update-dialog.component';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css'],
})
export class TodoComponent {
  @Input() todo: ITodo;
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;
  loading = false;

  constructor(
    private readonly todosService: TodosService,
    private readonly dialog: MatDialog,
  ) {}

  deleteTodo() {
    this.loading = true;
    this.todosService
      .deleteTodo(this.todo.id)
      .finally(() => (this.loading = false));
  }

  changeDone(done: boolean) {
    this.loading = true;
    const { text, title, id } = this.todo;
    this.todosService
      .updateTodo(id, {
        done,
        text,
        title,
      })
      .finally(() => (this.loading = false));
  }

  openUpdateDialog() {
    this.dialog.open(TodoUpdateDialogComponent, {
      data: this.todo,
    });
  }
}
