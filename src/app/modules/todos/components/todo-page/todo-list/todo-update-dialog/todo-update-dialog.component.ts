import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ITodo } from '../../../../todo.type';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InputErrorMatcher } from '../../../../../../directives/input-error-matcher.directive';
import { TodosService } from '../../../../todos.service';

@Component({
  selector: 'app-todo-update-dialog',
  templateUrl: './todo-update-dialog.component.html',
  styleUrls: ['./todo-update-dialog.component.css'],
})
export class TodoUpdateDialogComponent {
  updateTodoForm: FormGroup;
  inputErrorMatcher = new InputErrorMatcher();
  loading = false;

  constructor(
    private readonly todosService: TodosService,
    public dialogRef: MatDialogRef<TodoUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ITodo,
  ) {
    this.updateTodoForm = new FormGroup({
      title: new FormControl(data.title),
      text: new FormControl(data.text, [
        Validators.required,
        Validators.maxLength(260),
      ]),
    });
  }

  updateTodo = () => {
    this.loading = true;
    this.todosService
      .updateTodo(this.data.id, this.updateTodoForm.value)
      .then(this.closeModal)
      .finally(() => (this.loading = false));
  };

  closeModal = () => {
    this.dialogRef.close();
  };
}
