import { Component, OnInit, OnDestroy } from '@angular/core';
import { TodosStore } from '../../../../../store/todos.store';
import { ITodo } from '../../../todo.type';
import { Subscription } from 'rxjs';
import { TodosService } from '../../../todos.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: [
    './todo-list.component.css',
    '../../../../../styles/card.styles.css',
  ],
})
export class TodoListComponent implements OnInit, OnDestroy {
  todos: ITodo[] = [];
  subscriptions: Subscription[] = [];
  loading = false;
  filterButtons = [
    { text: 'All', value: undefined },
    { text: 'Done', value: true },
    { text: 'Not Done', value: false },
  ];
  filter?: boolean;

  constructor(
    private readonly todosStore: TodosStore,
    private readonly todosService: TodosService,
  ) {}

  ngOnInit() {
    this.subscribeToTodosUpdate();
    this.fetchTodoList();
  }

  ngOnDestroy(): void {
    for (const subscriptions of this.subscriptions) {
      subscriptions.unsubscribe();
    }
  }

  subscribeToTodosUpdate() {
    const todoSubscription = this.todosStore.todos.subscribe(
      todos => (this.todos = [...todos]),
    );
    this.subscriptions.push(todoSubscription);
  }

  fetchTodoList() {
    this.loading = true;
    this.todosService.fetchTodoList().finally(() => {
      this.loading = false;
    });
  }

  setFilter(value?: boolean) {
    this.filter = value;
  }
}
