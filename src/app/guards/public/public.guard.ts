import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { AuthStore } from '../../store/auth.store';
import { TODOS_ROUTE_PREFIX } from 'src/app/app.constants';

@Injectable({
  providedIn: 'root',
})
export class PublicGuard implements CanActivate {
  constructor(
    private readonly authStore: AuthStore,
    private readonly router: Router,
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): boolean {
    const token = this.authStore.getToken();
    if (token) {
      this.router.navigate([TODOS_ROUTE_PREFIX]);
      return false;
    }
    return true;
  }
}
