import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { AuthStore } from '../../store/auth.store';
import { SIGN_IN_ROUTE } from '../../modules/auth/auth.constants';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly authStore: AuthStore,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): boolean {
    const token = this.authStore.getToken();
    if (!token) {
      this.router.navigate([SIGN_IN_ROUTE]);
      return false;
    }
    return true;
  }
}
