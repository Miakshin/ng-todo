import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AUTH_ROUTE_PREFIX, TODOS_ROUTE_PREFIX } from './app.constants';
import {
  AUTH_ENDPOINT,
  REGISTRATION_SUCCESS,
  CONFIRM_REGISTRATION,
} from './modules/auth/auth.constants';
import { PublicGuard } from './guards/public/public.guard';
import { AuthGuard } from './guards/auth/auth.guard';
import { RegistrationSuccessPageComponent } from './modules/auth/components/registration-success-page/registration-success-page.component';
import { ConfirmEmailPageComponent } from './modules/auth/components/confirm-email-page/confirm-email-page.component';

const routes: Routes = [
  {
    path: REGISTRATION_SUCCESS,
    component: RegistrationSuccessPageComponent,
    canActivate: [PublicGuard],
  },
  {
    path: CONFIRM_REGISTRATION,
    component: ConfirmEmailPageComponent,
    canActivate: [PublicGuard],
  },
  {
    path: AUTH_ROUTE_PREFIX,
    loadChildren: () =>
      import('./modules/auth/auth.module').then(m => m.AuthModule),
    canActivate: [PublicGuard],
  },
  {
    path: TODOS_ROUTE_PREFIX,
    loadChildren: () =>
      import('./modules/todos/todos.module').then(m => m.TodosModule),
    canActivate: [AuthGuard],
  },
  { path: '**', redirectTo: AUTH_ENDPOINT },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
